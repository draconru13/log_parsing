﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.Text.RegularExpressions;

namespace WindowsFormsApp11
{
    public partial class Form1 : Form
    {
        public Form1(Form2 f)
        {
            InitializeComponent();
        }

       

        public string s;
        public string signatura;

        private void button1_Click(object sender, EventArgs e)
        {
            s = richTextBox1.Text;
            textBox1.Text = "";
            Random rnd = new Random();
            int red = rnd.Next(0, byte.MaxValue + 1);
            int green = rnd.Next(0, byte.MaxValue + 1);
            int blue = rnd.Next(0, byte.MaxValue + 1);
            signatura = comboBox1.Text;


            Regex regex = new Regex(@signatura, RegexOptions.IgnoreCase);
            MatchCollection matches = regex.Matches(s);
            textBox1.Text =  "В тексте: " + "\r\n";

            if (matches.Count > 0)
            {
                foreach (Match m in matches)
                {
                    SetSelectionStyle(m.Index, m.Length, FontStyle.Underline, red, green, blue);
              
                }
            }
            else
            {
                textBox1.Text = "Совпадений не найдено";
            }
            textBox1.Text += $"{signatura} появилось в тексте {matches.Count} раз(-а)  " + "\r\n";
        }
        void SetSelectionStyle(int startIndex, int length, FontStyle style, int r, int g, int b)
        {
            richTextBox1.Select(startIndex, length);
            richTextBox1.SelectionFont = new Font(richTextBox1.SelectionFont, richTextBox1.SelectionFont.Style | style);
            richTextBox1.SelectionColor = Color.FromArgb(r, g, b);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "Text files (*.txt)|*.txt|All files(*.*)|*.*";
            if (openFileDialog1.ShowDialog() == DialogResult.Cancel)
                return;
            string filename = openFileDialog1.FileName;
            string fileText = System.IO.File.ReadAllText(filename);
            richTextBox1.Text = fileText;
            string s = fileText;
            string signatura = comboBox1.Text;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Form2 main = this.Owner as Form2;
            s = main.richTextBox1.Text;
            textBox1.Text = "";
            Random rnd = new Random();
            int red = rnd.Next(0, byte.MaxValue + 1);
            int green = rnd.Next(0, byte.MaxValue + 1);
            int blue = rnd.Next(0, byte.MaxValue + 1);
            signatura = "Form7.cs";


            Regex regex = new Regex(@signatura, RegexOptions.IgnoreCase);
            MatchCollection matches = regex.Matches(s);
            textBox1.Text =  "В тексте: " + "\r\n";

            if (matches.Count > 0)
            {
                foreach (Match m in matches)
                {
                    SetSelectionStyle_(m.Index, m.Length, FontStyle.Underline, red, green, blue);
                 
                }
            }
            else
            {
                textBox1.Text = "Совпадений не найдено";
            }
            textBox1.Text += $"{signatura} появилось в тексте {matches.Count} раз(-а)  " + "\r\n";
        }
        void SetSelectionStyle_(int startIndex, int length, FontStyle style, int r, int g, int b)
        {
            Form2 main = this.Owner as Form2;
            main.richTextBox1.Select(startIndex, length);
            main.richTextBox1.SelectionFont = new Font(richTextBox1.SelectionFont, richTextBox1.SelectionFont.Style | style);
            main.richTextBox1.SelectionColor = Color.FromArgb(r, g, b);
        }

        public static string random_1()
        {
            Random res = new Random();

            String str = "123456789abcdefghijklmnopqrstuvwxyzZXCVBNMASDFGHJKLQWERTYUIOP";
            //этот размер генерировать автоматически
            int size = 189;

            String ran = "";

            for (int i = 0; i < size; i++)
            {

                int x = res.Next(26);
                ran = ran + str[x];
            }
            return ran;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Stopwatch watch = new Stopwatch();
            watch.Start();
            Form2 main = this.Owner as Form2;

            s = main.richTextBox1.Text;

            textBox1.Text = "";
            Random r = new Random();
            int red = r.Next(0, byte.MaxValue + 1);
            int green = r.Next(0, byte.MaxValue + 1);
            int blue = r.Next(0, byte.MaxValue + 1);
            Random size = new Random();

            //вместо слова form должно писать часть вируса
            //signatura = "Form";
            string s1 = "вирус";
            int len = s1.Length;



            //целое вирусное слово
            string s2 = random_1();

            StreamWriter incdate = new StreamWriter(@"C:\\Users\\Игорь\\Desktop\\WindowsFormsApp11\\test.txt");
            incdate.WriteLine(s2);
            incdate.Close();


            MessageBox.Show(s2);
            signatura = "";
            int word, counter = 0;
            for (int counter_1 = 0; counter_1 < 15; counter_1++)
            {
                word = size.Next(3, 10);
                if ((s2.Length - counter) < word)
                {
                    word = s2.Length - counter;
                }
                signatura = s2.Substring(counter, word); //надо выводить в форму

                counter = counter + word;
                Regex regex = new Regex(@signatura);
                Regex regex1 = new Regex(@s1);
                MatchCollection matches1 = regex1.Matches(s1);


                MatchCollection matches = regex.Matches(s);

                textBox1.Text = $"Вхождения " + signatura + " в исходном тексте: " + "\r\n";
                double sec = Convert.ToDouble(watch.ElapsedMilliseconds) / 1000;
                Random rand = new Random();
                if (matches.Count > 0)
                {
                    int sdvig = 0;
                    int count_match = 0;

                    foreach (Match m in matches)
                    {
                        count_match++;
                        int index = m.Index + sdvig;
                        main.richTextBox1.Text = main.richTextBox1.Text.Insert(index, s1);
                        sdvig += len;
                    }
                    s = main.richTextBox1.Text;
                    matches = regex.Matches(s);
                    foreach (Match m in matches)
                    {
                        SetSelectionStyle_(m.Index, m.Length, FontStyle.Underline, red, green, blue);
                    }
                }
                else
                {
                    textBox1.Text = "Совпадений не найдено";
                }

                textBox1.Text += $"Лабораторная работа 2: В форме {signatura} исключения появились {matches.Count} раз(-а)" + Environment.NewLine + $"Парссинг лог-журнала выполнен за {sec} секунд(-ы)";

                watch.Stop();
            }


            /*
            Regex regex = new Regex(@signatura);
            Regex regex1 = new Regex(@s1);
            MatchCollection matches1 = regex1.Matches(s1);


            MatchCollection matches = regex.Matches(s);

            textBox1.Text = $"Вхождения " + signatura + " в исходном тексте: " + "\r\n";
            double sec = Convert.ToDouble(watch.ElapsedMilliseconds) / 1000;
            Random rand = new Random();
            if (matches.Count > 0)
            {
                int sdvig = 0;
                int count_match = 0;

                foreach (Match m in matches)
                {
                    count_match++;
                    int index = m.Index + sdvig;
                    main.richTextBox1.Text = main.richTextBox1.Text.Insert(index, s1);
                    sdvig += len;
                }
                s = main.richTextBox1.Text;
                matches = regex.Matches(s);
                foreach (Match m in matches)
                {
                    SetSelectionStyle_(m.Index, m.Length, FontStyle.Underline, red, green, blue);
                }
            }
            */


            /*
            string x1 = "21.03,2022";
            string x1_number = x1.Substring(0, 2);
            string x1_month = x1.Substring(3, 2);
            string x1_year = x1.Substring(6, 4);

            string x2 = "25.03,2022";
            string x2_number = x2.Substring(0, 2);

            string x3 = "29.03,2022";
            string x3_number = x3.Substring(0, 2);

            string x4 = "01.04,2022";
            string x4_number = x4.Substring(0, 2);
            string x4_month = x4.Substring(3, 2);

            string x5 = "02.04,2022";
            string x5_number = x5.Substring(0, 2);


            string x6 = "04.04,2022";
            string x6_number = x6.Substring(0, 2);


            string x7 = "05.04,2022";
            string x7_number = x7.Substring(0, 2);


            string x8 = "15.04,2022";
            string x8_number = x8.Substring(0, 2);


            string x9 = "19.04,2022";
            string x9_number = x9.Substring(0, 2);


            string x_begin = textBox2.Text;
            string x_begin_number = x_begin.Substring(0, 2);
            string x_begin_number_pointer = x_begin.Substring(0, 5);
            string x_begin_month = x_begin.Substring(3, 2);
            string x_begin_year = x_begin.Substring(6, 4);

            string x_end = textBox3.Text;
            string x_end_number = x_end.Substring(0, 2);
            string x_end_number_pointer = x_end.Substring(0, 5);
            string x_end_month = x_end.Substring(3, 2);
            string x_end_year = x_end.Substring(6, 4);
            

            
            if (matches.Count > 0)
            {
                int sdvig = 0;
                int count_match = 0;

                foreach (Match m in matches){
                    count_match++;
                    int index = m.Index + sdvig + rand.Next(10, 30);

                    main.richTextBox1.Text = main.richTextBox1.Text.Insert(index, s1);
                    /*
                    if (x_begin_year == "2022" && x_end_year == "2022")
                    {
                        if (x_begin_number_pointer == "21.03" || x_begin_number_pointer == "22.03" || x_begin_number_pointer == "23.03" || x_begin_number_pointer == "24.03" || x_begin_number_pointer == "25.03" || x_begin_number_pointer == "26.03" || x_begin_number_pointer == "27.03" || x_begin_number_pointer == "28.03" || x_begin_number_pointer == "29.03" || x_begin_number_pointer == "30.03" || x_begin_number_pointer == "31.03" || x_begin_number_pointer == "1.04" || x_begin_number_pointer == "2.04" || x_begin_number_pointer == "3.04" || x_begin_number_pointer == "4.04" || x_begin_number_pointer == "5.04" || x_begin_number_pointer == "6.04" || x_begin_number_pointer == "7.04" || x_begin_number_pointer == "8.04" || x_begin_number_pointer == "9.04" || x_begin_number_pointer == "10.04" || x_begin_number_pointer == "11.04" || x_begin_number_pointer == "12.04" || x_begin_number_pointer == "13.04" || x_begin_number_pointer == "14.04" || x_begin_number_pointer == "15.04" || x_begin_number_pointer == "16.04" || x_begin_number_pointer == "17.04" || x_begin_number_pointer == "18.04")
                        {
                            main.richTextBox1.Text = main.richTextBox1.Text.Insert(index, s1);
                        }
                        else if (x_end_number_pointer == "22.03" || x_end_number_pointer == "23.03" || x_end_number_pointer == "24.03" || x_end_number_pointer == "25.03" || x_end_number_pointer == "26.03" || x_end_number_pointer == "27.03" || x_end_number_pointer == "28.03" || x_end_number_pointer == "29.03" || x_end_number_pointer == "30.03" || x_end_number_pointer == "31.03" || x_end_number_pointer == "1.04" || x_end_number_pointer == "2.04" || x_end_number_pointer == "3.04" || x_end_number_pointer == "4.04" || x_end_number_pointer == "5.04" || x_end_number_pointer == "6.04" || x_end_number_pointer == "7.04" || x_end_number_pointer == "8.04" || x_end_number_pointer == "9.04" || x_end_number_pointer == "10.04" || x_end_number_pointer == "11.04" || x_end_number_pointer == "12.04" || x_end_number_pointer == "13.04" || x_end_number_pointer == "14.04" || x_end_number_pointer == "15.04" || x_end_number_pointer == "16.04" || x_end_number_pointer == "17.04" || x_end_number_pointer == "18.04" || x_end_number_pointer == "19.04")
                        {
                            main.richTextBox1.Text = main.richTextBox1.Text.Insert(index, s1);
                        }
                        else if ((Convert.ToInt32(x1_month) >= Convert.ToInt32(x_begin_month)) && (Convert.ToInt32(x4_month) <= Convert.ToInt32(x_end_month)))
                        {
                            
                            if ((Convert.ToInt32(x1_number) > Convert.ToInt32(x_begin_number) && (Convert.ToInt32(x9_number) < Convert.ToInt32(x_end_number))))// || (Convert.ToInt32(x9_number) > Convert.ToInt32(x_begin_number) && (Convert.ToInt32(x1_number) > Convert.ToInt32(x_begin_number))))// || (Convert.ToInt32(x9_number) < Convert.ToInt32(x_begin_number) && (Convert.ToInt32(x1_number) > Convert.ToInt32(x_begin_number))))
                            {
                                main.richTextBox1.Text = main.richTextBox1.Text.Insert(index, s1);
                            }
                            else
                            {
                                MessageBox.Show("1) Вхождения не попали в интервал дат");
                                break;
                            }
                        }
                        else
                        {
                            MessageBox.Show("2) Вхождения не попали в интервал дат");
                            break;
                        }

                    }
                    else
                    {
                        MessageBox.Show("Логи ограничены поиском только за 2022 год");
                        break;
                    }
                    
            

                    sdvig += len; 
                    
                }

                s = main.richTextBox1.Text;
                matches = regex.Matches(s);
                foreach (Match m in matches)
                {
                    SetSelectionStyle_(m.Index, m.Length, FontStyle.Underline, red, green, blue);
                }
                

        }*/
            /*
            else
            {
                textBox1.Text = "Совпадений не найдено";
            }

            textBox1.Text += $"Лабораторная работа 2: В форме {signatura} исключения появились {matches.Count} раз(-а)" + Environment.NewLine + $"Парссинг лог-журнала выполнен за {sec} секунд(-ы)";

            watch.Stop();
            */
        }
        private void SetSelectionStyle_1(int startIndex, int length, FontStyle style, int r, int g, int b)
        {
            Form2 main = this.Owner as Form2;
            main.richTextBox1.Select(startIndex, length);
            main.richTextBox1.SelectionFont = new Font(richTextBox1.SelectionFont, main.richTextBox1.SelectionFont.Style | style);
            main.richTextBox1.SelectionColor = Color.FromArgb(r, g, b);

        }

        private void button5_Click(object sender, EventArgs e)
        {
            Form2 main = this.Owner as Form2;
            s = main.richTextBox1.Text;
            textBox1.Text = "";
            Random rnd = new Random();
            int red = rnd.Next(0, byte.MaxValue + 1);
            int green = rnd.Next(0, byte.MaxValue + 1);
            int blue = rnd.Next(0, byte.MaxValue + 1);
            signatura = "Form4.cs";


            Regex regex = new Regex(@signatura, RegexOptions.IgnoreCase);
            MatchCollection matches = regex.Matches(s);
            textBox1.Text = "В тексте: " + "\r\n";

            if (matches.Count > 0)
            {
                foreach (Match m in matches)
                {
                    SetSelectionStyle_(m.Index, m.Length, FontStyle.Underline, red, green, blue);

                }
            }
            else
            {
                textBox1.Text = "Совпадений не найдено";
            }
            textBox1.Text += $"{signatura} появилось в тексте {matches.Count} раз(-а)  " + "\r\n";
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void button6_Click(object sender, EventArgs e)
        {
            string str = "lab_work_4";
            int i = 0;
            while (i <= richTextBox1.Text.Length - str.Length)
            {
                //выделение цветом
                i = richTextBox1.Text.IndexOf(str, i);
                if (i < 0)
                {
                    break;
                }
                richTextBox1.SelectionStart = i;
                richTextBox1.SelectionLength = str.Length;
                richTextBox1.SelectionColor = Color.Red;
                i += str.Length;
            }
            MessageBox.Show("Файл хуйня");
        }


    }
}
