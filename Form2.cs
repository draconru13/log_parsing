﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp11
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void лр4ToolStripMenuItem_Click(object sender, EventArgs e)
        {

            Form1 программа = new Form1(this);
            программа.Owner = this;
            программа.ShowDialog();
        }

        private void файлToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "Text files (*.txt)|*.txt|All files(*.*)|*.*";
            if (openFileDialog1.ShowDialog() == DialogResult.Cancel)
                return;
            string filename = openFileDialog1.FileName;
            string fileText = System.IO.File.ReadAllText(filename);
            richTextBox1.Text = fileText;
          
        }

        private void Form2_FormClosed(object sender, FormClosedEventArgs e)
        {
            Properties.Settings.Default.bup = this.richTextBox1.Text;
            Properties.Settings.Default.Save();
        }
        private void abc()
        {
            this.richTextBox1.Text = Properties.Settings.Default.bup;
            this.Text = this.richTextBox1.Text;
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            this.abc();
        }
    }
}
